# Appraisal System
This project is based on laravel framework.
Admin has all privilege where as employee has certain restriction to access.By default You can register as an employee:
http://127.0.0.1:8000/register

## You can engage in this project to make this more advanced and you can add new features.You contribution will be highly appreciated !!


## Installation
This project is for employee management.Employee can register himself and employee can assign a salary for employee...

## Clone the repo
git clone https://satriaaca@bitbucket.org/satriaaca/appraisal-system.git

## Composer install
cd appraisal-system
composer install

# Database
Ajust the database information, then:

php artisan migrate

# Seed the database:

php artisan db:seed

# Login credentials
Admin Account: ['email' => 'admin@admin.com', 'password' => 'admin123']

Employee Account: ['email' => 'employee@gmail.com', 'password' => 'employee123']

Manager Account: ['email' => 'manager@gmail.com', 'password' => 'manager123'] 