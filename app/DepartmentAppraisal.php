<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentAppraisal extends Model
{
    protected $table = 'department_appraisal';
    protected $fillable = ['user_id','survey_id','manager_id','is_filled'];
    //
}
