<?php

namespace App\Http\Controllers;

use App\Appraisal;
use App\DepartmentAppraisal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use DB;

class AppraisalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->redirectTo('admin/survey');
        //
    }

    public function survey()
    {

    }

    public function updateSurvey(Request $request)
    {
        $id = Auth::id();
        $app = Appraisal::latest()->first();
        $sid = $app->id;
        $appr = Appraisal::find($sid);
        $appr->user_id = $id;
        $appr->save();
        //-----------------
        $dp = new DepartmentAppraisal();
        $dp->user_id = $id;
        $dp->survey_id = $sid;
        $dp->manager_id = '6';
        $dp->is_filled = '0';
        $dp->save();
        // -----------------
        Toastr::success('Successfully updated!','Success');
        return redirect()->route('dashboard');
        //return view('admin.appraisal.appraisal', compact('id','id'));
        # code...
    }
    public function updateFill($id){
        //-----------------
        $dp = DepartmentAppraisal::find($id);
        $dp->is_filled = '1';
        $dp->save();
        return redirect()->route('survey.appraisal');

    }

    public function viewAll(){
        $apps = Appraisal::query()
                ->join('users','users.id', '=', 'survey_results.user_id')
                ->get()
                ->all();
        return view('admin.appraisal.index',compact('apps'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function departmentApp(){
        $data = DB::select(DB::raw("SELECT *, `department_appraisal`.`id` as `dep_id` from `department_appraisal` inner join `users` on `users`.`id` = `department_appraisal`.`user_id`"));
        return view('admin.appraisal.depapp', compact('data'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function show(Appraisal $appraisal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function edit(Appraisal $appraisal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appraisal $appraisal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appraisal  $appraisal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appraisal $appraisal)
    {
        //
    }
}
