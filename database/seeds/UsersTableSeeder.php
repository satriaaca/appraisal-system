<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'created_at' => \Carbon\Carbon::now(),
                'username' => 'gunner',
                'image' => '',
                'first_name' => 'gunner',
                'last_name' => 'kc',
                'role' => 'manager',
                'email' => 'manager@gmail.com',
                'password' => bcrypt('manager123'),
                'status' => '1',
                'phone' => '986133131',
                'address' => 'butwal',
                'gender' => 'male',
                'dob' => '2019-03-12',
                'join_date' => '2019-03-12',
                'job_type' => 'IT',
                'city' => 'butwal',
                'age' => '22',
            ],
        ]);
    }
}
