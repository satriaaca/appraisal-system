@extends('admin.layout.master')

@section('content')

    <div id="main-wrapper">

    @include('admin.includes.sidebar')
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Appraisal Management</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="{{route('appraisal')}}">Appraisal</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-2">
                        <a class="btn btn-lg btn-dark" href="{{route('admin.survey')}}">Add Appraisal</a>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Appraisal List</h5>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>S.N.</th>
                                            <th>Employee Name</th>
                                            <th>Is Filed</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($data as $datas)
                                        <tr>
                                            <td>{{$loop -> index+1 }}</td>
                                            <td>{{$datas->first_name}} {{ $datas->last_name}}</td>
                                            <td>
                                            @if ($datas->is_filled == 1)
                                                <span class="badge badge-success">Yes</span>
                                            @else
                                                <span class="badge badge-danger">No</span>
                                            @endif
                                            
                                            </td>
                                            <td>
                                            @if ($datas->is_filled == 0)
                                                <a href="{{route('update.fill',$datas->dep_id)}}" class="btn btn-success">Fill Appraisal</a>
                                                @else
                                                <span></span>
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <footer class="footer text-center">
                All Rights Reserved &copy 2019
            </footer>
        </div>
    </div>

@endsection